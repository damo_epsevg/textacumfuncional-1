package edu.upc.damo;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    EditText camp;
    TextView res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();


    }

    private void inicialitza() {
        camp =  findViewById(R.id.camp);
        res =  findViewById(R.id.resultat);

        camp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return nouContingut(v, actionId, event);
            }
        });


    }

    private void esborraResultat() {
        res.setText("");
        camp.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_ajut:
                return true;
            case R.id.action_afegeix:
                return true;
            case R.id.action_esborra_resultat:
                esborraResultat();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean nouContingut(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)
        switch (actionId){
            case EditorInfo.IME_ACTION_GO:
                afegeixNouText();
                amagaTeclat(v);
                return true;
        }

        // Mirem quinevent s'ha generat
        // (Cas de teclat hard)
        switch (event.getAction()) {
            case KeyEvent.ACTION_UP:
                afegeixNouText();
                return true;
        }
        return true;
    }

    private void afegeixNouText() {
        afegeixAResultat(camp.getText());
        camp.setText("");
    }

    private void amagaTeclat(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(),0);
    }

    private void afegeixAResultat(Editable text) {
        res.append("\n");
        res.append(text);
    }
}


